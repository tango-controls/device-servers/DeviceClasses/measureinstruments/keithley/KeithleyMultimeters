// ============================================================================
//
// = CONTEXT
//    TANGO Project - KeithleyMultimeters Support Library
//
// = FILENAME
//    KeithleyManager.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/utils/XString.h>  //- for data conversion (str -> num)
#include <yat/utils/String.h>   //- to_upper
#include <yat/time/Timer.h>     //- perf
#include <yat/Portability.h>    //- NaN
#include "KeithleyManager.hpp"

// ----------------------------------------------------------------------------
//- the YAT user messages 
const std::size_t SET_INTEGRATION_RATE_MSG		= yat::FIRST_USER_MSG + 10;
// const std::size_t GET_INTEGRATION_RATE_MSG		= yat::FIRST_USER_MSG + 11;
const std::size_t SET_BUFFER_SIZE_MSG     		= yat::FIRST_USER_MSG + 20;
// const std::size_t GET_BUFFER_SIZE_MSG     		= yat::FIRST_USER_MSG + 21;
const std::size_t GET_BUFFERED_DATA_SIZE_MSG	= yat::FIRST_USER_MSG + 22;
const std::size_t SET_TRIGGER_SRC_MSG		      = yat::FIRST_USER_MSG + 30;
// const std::size_t GET_TRIGGER_SRC_MSG		      = yat::FIRST_USER_MSG + 31;
const std::size_t ABORT_ACQUISITION_MSG    		= yat::FIRST_USER_MSG + 40;
const std::size_t CONFIGURE_ACQUISITION_MSG		= yat::FIRST_USER_MSG + 50;
const std::size_t START_ACQUISITION_MSG				= yat::FIRST_USER_MSG + 60;
const std::size_t READ_BUFFERED_DATA_MSG			= yat::FIRST_USER_MSG + 70;
const std::size_t UPDATE_STATE_STATUS_MSG			= yat::FIRST_USER_MSG + 80;
const std::size_t WRITE_READ_MSG							= yat::FIRST_USER_MSG + 90;
// ----------------------------------------------------------------------------
//- Thread polling period (when enabled)
static const std::size_t kPERIODIC_TMOUT_MS		= 300;
//- message timeout value
static const std::size_t kMSG_TIMEOUT					= 2000;
// ----------------------------------------------------------------------------
//- commands
const std::string IDENTIFICATION_CMD				= "*IDN?";
//- Configure Keithley in Voltmeter
const std::string SET_VOLTMETER_CMD       	= "CONF:VOLT";
//- Read back stored data
const std::string READ_DATA_CMD       		 	= "TRAC:DATA?";
//- Acquisition buffer size
const std::string GET_BUFF_SIZE_CMD       	= "TRAC:POIN?";
const std::string SET_BUFF_SIZE_CMD       	= "TRAC:POIN ";
const std::string ACTUAL_BUFF_SIZE_CMD      = "TRAC:POIN:ACT?";
//- Integration rate (nb Power Line Cycle)
const std::string GET_INTEGRATION_RATE_CMD	= "VOLT:DC:NPLC?";
const std::string SET_INTEGRATION_RATE_CMD	= "VOLT:DC:NPLC ";
//- Acquisition
const std::string ABORT_ACQUISITION_CMD			= "INIT:CONT OFF";
const std::string START_ACQUISITION_CMD			= "INIT:CONT ON";
//- Feed buffer
const std::string STORE_DATA_CMD						= "TRAC:FEED:CONT NEXT";
const std::string CLEAR_DATA_CMD						= "TRAC:CLE";
//- Trigger Source
const std::string SET_TRIGGER_SRC_CMD				= "TRIG:SOUR ";
const std::string GET_TRIGGER_SRC_CMD				= "TRIG:SOUR?";
//- instrument error query
const std::string GET_SYSTEM_ERR_CMD				= "SYST:ERR?";
// ----------------------------------------------------------------------------
//- commands lines begining with '#' are comments: do not send!
const std::string COMMENT 									= "#";
// ----------------------------------------------------------------------------
const std::string NO_ERROR									=	"No error";
// ----------------------------------------------------------------------------

namespace KeithleyMultimeters_ns
{

// ============================================================================
// KeithleyManager::KeithleyManager
// ============================================================================
KeithleyManager::KeithleyManager (std::string& comLink_device_name,
                   								std::vector<std::string>& user_config,
																	Tango::DeviceImpl * host_dev)
:	yat4tango::DeviceTask(host_dev),
	m_hostDev(host_dev),
	m_comLink(0),
	m_comName(comLink_device_name),
	m_keithley_data(0),
	m_state(Tango::UNKNOWN),
	m_status(""),
	m_error(NO_ERROR),
	m_user_config(user_config),
	m_bufferedDataStr(""),
	m_nbData(0),
	m_buffSize(0),
	m_nplc(0.),
	m_triggerSrc("EXTERNAL"),
	m_cmd_getData_sent(false),
  m_acquisition_on_the_way(false)
{
	//- std::cout << "\t\tKeithleyManager::KeithleyManager <-" << std::endl;

	//- std::cout << "\t\tKeithleyManager::KeithleyManager ->" << std::endl;
}

// ============================================================================
// KeithleyManager::~KeithleyManager
// ============================================================================
KeithleyManager::~KeithleyManager (void)
{
	//- std::cout << "\t\tKeithleyManager::~KeithleyManager <-" << std::endl;

	//- std::cout << "\t\tKeithleyManager::~KeithleyManager ->" << std::endl;
}

// ============================================================================
//- the user core of the Task
// ============================================================================
void KeithleyManager::process_message (yat::Message& _msg) throw (Tango::DevFailed)
{
	//- The DeviceTask's lock_ -------------
	// DEBUG_STREAM << "KeithleyManager::process_message::receiving msg " << _msg.to_string() << std::endl;

	//- handle msg
	switch (_msg.type())
	{
		//- THREAD_INIT =======================
	case yat::TASK_INIT:
		{
			//- "initialization" code goes here
			try
			{
				//- create proxy
				create_proxy(m_comName);
				//- create Keithley data parser
				m_keithley_data = new KeithleyData(m_hostDev);
				m_keithley_data->go();
				//- configure task
				enable_timeout_msg(false);
				enable_periodic_msg(true);
				set_periodic_msg_period(kPERIODIC_TMOUT_MS);
				//- Clear vector
				{
					yat::MutexLock gard(m_data_mutex);
					m_vector_data.clear();
				}
				//- Refresh state/status
				m_state = Tango::STANDBY;
				m_status = "Device initialized";
				m_error = NO_ERROR;

				INFO_STREAM << " KeithleyManager::handle_message handling TASK_INIT KeithleyManager initialised. [" << m_comLink << "]" << std::endl;
//- std::cout << " KeithleyManager::handle_message handling TASK_INIT KeithleyManager initialised. " << std::endl;
			}
			catch (std::bad_alloc&)
			{
				m_error = "KeithleyManager::TASK_INIT OUT_OF_MEMORY cannot allocate device proxy ";
				FATAL_STREAM << "TASK_INIT -> FATAL bad_alloc : " << m_error << std::endl;
				return;
			}
			catch(Tango::DevFailed& df)
			{
				m_error = "KeithleyManager::TASK_INIT : DevFailed exception caught trying to initialise KeithleyManager.";
				FATAL_STREAM << m_error << std::endl;
				FATAL_STREAM << df << std::endl;
				return;
			}
			catch(...)
			{
				m_error = "KeithleyManager::TASK_INIT :  exception ... caught trying to initialise KeithleyManager.";
				FATAL_STREAM << "TASK_INIT -> FATAL [...] : " << m_error << std::endl;
				return;
			}
		}
		break;
		//- TASK_EXIT =======================
	case yat::TASK_EXIT:
		{
			INFO_STREAM << "KeithleyManager::handle_message handling TASK_EXIT thread is quitting ..." << std::endl;
			//- abort acquisition
			abort_scan();
			//- clear stored data
			reset_stored_data();
			//- "release" code goes here
			delete_proxy();
		}
		break;
		//- TASK_PERIODIC ===================
	case yat::TASK_PERIODIC:
		{
			try
			{
	      if ( !m_acquisition_on_the_way )
	      {
	        update_internal_data();
	        update_nb_stored_data();
	      }
	      else
	      {
	        upload_buffered_data();
	      }
    	}
			catch(yat::Exception& yex)
			{
				_YAT_TO_TANGO_EXCEPTION(yex, tex);
				m_error = "Failed to update data, caught [YAT exception]";
				ERROR_STREAM << "KeithleyManager::handle_message TASK_PERIODIC failed: " << tex << std::endl;
			}
		}
		break;
    //- USER_DEFINED_MSG ================
  case SET_INTEGRATION_RATE_MSG:
    {
      std::string nplcStr = _msg.get_data<std::string> ();

    	std::string cmd_to_send = SET_INTEGRATION_RATE_CMD + nplcStr;
    	send_receive(cmd_to_send);
    }
    break;
  case SET_BUFFER_SIZE_MSG:
    {
      std::string bSizeStr = _msg.get_data<std::string> ();

    	std::string cmd_to_send = SET_BUFF_SIZE_CMD + bSizeStr;
    	send_receive(cmd_to_send);
    }
    break;
  case SET_TRIGGER_SRC_MSG:
    {
      std::string trigSrcStr = _msg.get_data<std::string> ();

    	std::string cmd_to_send = SET_TRIGGER_SRC_CMD + trigSrcStr;
    	send_receive(cmd_to_send);
    }
    break;
  case CONFIGURE_ACQUISITION_MSG:
    {
			configure();
		}
    break;
  case START_ACQUISITION_MSG:
    {
//- std::cout << "\n\n\t\t START ACQ ..." << std::endl;
			//- clear stored data
			reset_stored_data();
//- std::cout << "\t\t START ACQ -> reset" << std::endl;
  		//- continuous reading
			send_receive(START_ACQUISITION_CMD);
//- std::cout << "\t\t START ACQ -> start acq" << std::endl;
			//- start monitoring 
			// readBufferedData();
//- std::cout << "\t\t START ACQ -> read buff data" << std::endl;
			//- start acquition : feed buffer
			send_receive(STORE_DATA_CMD);
// std::cout << "\t\t START ACQ <-" << std::endl;
//- std::cout << "\t\t START ACQ -> sent!" << std::endl;
    }
    break;
  case ABORT_ACQUISITION_MSG:
    {
    	static std::string cmd_to_send(ABORT_ACQUISITION_CMD);
    	send_receive(cmd_to_send);
    	//- clear store data
    	//reset_stored_data(); //- TODO : check if cmd must be sent!
      //- update internal data
      m_acquisition_on_the_way = false;
    }
    break;
  case READ_BUFFERED_DATA_MSG:
    {
      //- clear previous converted data
      {
        yat::MutexLock gard(m_data_mutex);
        //- clear previous raw received data (Keithley send data in ASCII)
        m_bufferedDataStr.clear();
        m_nbData = 0;
        m_buffSize = 0;
        m_cmd_getData_sent = false;
        m_vector_data.clear();
      }
			//- get the number of buffered data
			std::string nbDatastr("");
			nbDatastr = send_receive(GET_BUFF_SIZE_CMD); //- nb data configured
		// std::cout << "\t\t RBD -> nbDataStr = " << nbDatastr << std::endl;
			{
				yat::MutexLock gard(m_com_mutex);
				if( !nbDatastr.empty() )
				{
					m_nbData = yat::XString<size_t>::to_num(nbDatastr);
				}
			}
//- std::cout << "\t\t RBD MSG -> m_nbData = " << m_nbData << " & m_buffSize = " << m_buffSize << "\n" << std::endl;
      //- do not update internal data
      m_acquisition_on_the_way = true;
    }
    break;
  case UPDATE_STATE_STATUS_MSG:
	  {
	  	get_error();
	  }
	  break;
  case WRITE_READ_MSG:
	  {
			//- extract cmd
      WriteRead_Str & dataStr = _msg.get_data<WriteRead_Str>();

      dataStr.response = send_receive(dataStr.cmd_to_send);
	  }
	  break;
	}//- switch (_msg.type())
} //- KeithleyManager::handle_message

// ===================================================================
// KeithleyManager::update_internal_data
// ===================================================================
void KeithleyManager::update_internal_data()
{
	//- update integration rate
  std::string nplcStr = send_receive(GET_INTEGRATION_RATE_CMD);
	//- update user nb points
	std::string nbDatastr = send_receive(GET_BUFF_SIZE_CMD);
  {
  	yat::MutexLock gard(m_data_mutex);
  	m_nplc = m_nbData = yat::IEEE_NAN;
    //- update trigger source
    m_triggerSrc = send_receive(GET_TRIGGER_SRC_CMD);
    if( !nplcStr.empty() )
    {
    	m_nplc = yat::XString<double>::to_num(nplcStr);
    }
    if( !nbDatastr.empty() )
    {
    	m_nbData = yat::XString<double>::to_num(nbDatastr);
    }
  }
}

// ===================================================================
// KeithleyManager::update_nb_stored_data
// ===================================================================
void KeithleyManager::update_nb_stored_data()
{
  std::string buff_size_str = send_receive(ACTUAL_BUFF_SIZE_CMD);
  yat::MutexLock gard(m_data_mutex);
  m_buffSize = 0;
  if( !buff_size_str.empty() )
  {
  	m_buffSize = yat::XString<size_t>::to_num(buff_size_str);
  }
  // std::cout << "\t\t update_nb_stored_data -> m_buffSize = " << m_buffSize << std::endl;
}

// ===================================================================
// KeithleyManager::set_buffer_size
// ===================================================================
void KeithleyManager::set_buffer_size(std::size_t size)
{
	if ( !m_comLink )
	{
		Tango::Except::throw_exception ("BAD_INITIALIZATION",
			"No communication link built!",
			"KeithleyManager::set_buffer_size");
	}
	
	std::string sizeStr = yat::XString<size_t>::to_string(size);
  yat::Message* msg = yat::Message::allocate(SET_BUFFER_SIZE_MSG);
  msg->attach_data(sizeStr);
  post(msg);
}

// ===================================================================
// KeithleyManager::get_buffer_size
// ===================================================================
std::size_t KeithleyManager::get_buffer_size()
{
  yat::MutexLock gard(m_data_mutex);
  return m_nbData;
}

// ===================================================================
// KeithleyManager::get_actual_nbPoints
// ===================================================================
std::size_t KeithleyManager::get_actual_nbPoints()
{
  // bool waitable = true;
  // yat::Message* msg = yat::Message::allocate(GET_BUFFERED_DATA_SIZE_MSG, MAX_USER_PRIORITY, waitable);
  // wait_msg_handled(msg, kMSG_TIMEOUT);
  yat::MutexLock gard(m_data_mutex);
  return m_buffSize;
}

// ===================================================================
// KeithleyManager::set_nplc
// ===================================================================
void KeithleyManager::set_nplc(double nplc)
{
	if ( !m_comLink )
	{
		Tango::Except::throw_exception ("BAD_INITIALIZATION",
			"No communication link built!",
			"KeithleyManager::set_nplc");
	}
	
	std::string nplcStr = yat::XString<double>::to_string(nplc);
  yat::Message* msg = yat::Message::allocate(SET_INTEGRATION_RATE_MSG);
  msg->attach_data(nplcStr);
  post(msg);
}

// ===================================================================
// KeithleyManager::get_nplc
// ===================================================================
double KeithleyManager::get_nplc()
{
	// bool waitable = true;
 //  yat::Message* msg = yat::Message::allocate(GET_INTEGRATION_RATE_MSG, MAX_USER_PRIORITY, waitable);
 //  wait_msg_handled(msg, kMSG_TIMEOUT);
  yat::MutexLock gard(m_data_mutex);
  return m_nplc;
}

// ===================================================================
// KeithleyManager::set_trigger_source
// ===================================================================
void KeithleyManager::set_trigger_source(std::string trgSrc)
{
	if ( !m_comLink )
	{
		Tango::Except::throw_exception ("BAD_INITIALIZATION",
			"No communication link built!",
			"KeithleyManager::set_trigger_source");
	}
	
  //- convert in upper case
  yat::StringUtil::to_upper(&trgSrc);
  
  //- check value
  if ( trgSrc.find("IMM") == std::string::npos &&
       trgSrc.find("TIM") == std::string::npos &&
       trgSrc.find("MAN") == std::string::npos &&
       trgSrc.find("BUS") == std::string::npos &&
       trgSrc.find("EXT") == std::string::npos )
  {
  		Tango::Except::throw_exception ("DATA_OUT_OF_RANGE",
			                                "Trigger source should be : IMM, TIM, MAN, BUS or EXT!",
			                                "KeithleyManager::set_trigger_source");
  }
        
  yat::Message* msg = yat::Message::allocate(SET_TRIGGER_SRC_MSG);
  msg->attach_data(trgSrc);
  post(msg);
}

// ===================================================================
// KeithleyManager::get_trigger_source
// ===================================================================
std::string KeithleyManager::get_trigger_source()
{
	// bool waitable = true;
 //  yat::Message* msg = yat::Message::allocate(GET_TRIGGER_SRC_MSG, MAX_USER_PRIORITY, waitable);
 //  wait_msg_handled(msg, kMSG_TIMEOUT);
  yat::MutexLock gard(m_data_mutex);
  return m_triggerSrc;
}

// ===================================================================
// KeithleyManager::configure_scan
// ===================================================================
void KeithleyManager::configure_scan()
{
	if ( !m_comLink )
	{
		Tango::Except::throw_exception ("BAD_INITIALIZATION",
			"No communication link built!",
			"KeithleyManager::configure_scan");
	}
	
  yat::Message* msg = yat::Message::allocate(CONFIGURE_ACQUISITION_MSG);
  post(msg);
}

// ===================================================================
// KeithleyManager::configure
// ===================================================================
void KeithleyManager::configure()
{
	std::size_t config_size = m_user_config.size();

	//- send user defined configuration
	for(std::size_t idx=0; idx < config_size; idx++)
	{
		//- check is not a comment
		if (m_user_config.at(idx).find(COMMENT) == std::string::npos)
		{
			std::string cmd = m_user_config.at(idx);
			send_receive(cmd);
//- std::cout << "\n\t\t CONF cmd *" << cmd << "* SENT." << std::endl;
		}
	}
}

// ===================================================================
// KeithleyManager::reset_stored_data
// ===================================================================
void KeithleyManager::reset_stored_data()
{
	send_receive(CLEAR_DATA_CMD);
}

// ===================================================================
// KeithleyManager::abort_scan
// ===================================================================
void KeithleyManager::abort_scan()
{
	if ( !m_comLink )
	{
		Tango::Except::throw_exception ("BAD_INITIALIZATION",
			"No communication link built!",
			"KeithleyManager::abort_scan");
	}
	
  yat::Message* msg = yat::Message::allocate(ABORT_ACQUISITION_MSG);
  post(msg);
}

// ===================================================================
// KeithleyManager::start_scan
// ===================================================================
void KeithleyManager::start_scan()
{
	if ( !m_comLink )
	{
		Tango::Except::throw_exception ("BAD_INITIALIZATION",
			"No communication link built!",
			"KeithleyManager::start_scan");
	}

	//- start acquisition
  yat::Message* msg1 = yat::Message::allocate(START_ACQUISITION_MSG);
  post(msg1);

  //-  read back data
  readBufferedData();
}

// ===================================================================
// KeithleyManager::update_state_status
// ===================================================================
void KeithleyManager::update_state_status()
{
	bool waitable = true;

  yat::Message* msg = yat::Message::allocate(UPDATE_STATE_STATUS_MSG, MAX_USER_PRIORITY, waitable);

  wait_msg_handled(msg, kMSG_TIMEOUT);
}

// ===================================================================
// KeithleyManager::get_error
// ===================================================================
void KeithleyManager::get_error()
{
  Tango::DevState state = Tango::UNKNOWN;
  std::string status("unknown");
  
	if ( !m_comLink )
	{
	  yat::MutexLock gard(m_stat_mutex);
		m_state  = Tango::FAULT;
		m_status = "Failed to create communication link proxy on " + m_comName;
		return;
	}

	try
	{
		//- check internal error(s)
		if ( m_error.find(NO_ERROR) == std::string::npos )
		{
			state  = Tango::ALARM;
			status = m_error;
		}
		else if ( m_acquisition_on_the_way && state != Tango::FAULT )
		{
      state  = Tango::RUNNING;
      if ( !m_cmd_getData_sent )
      {
				status = "Data acquisition on the way...";
      }
      else
      {
        status = "Reading back (and parsing) data...";
      }
		}
		else
		{
			//- read back instrument error(s)
			std::string syst_err = send_receive(GET_SYSTEM_ERR_CMD);
			if ( syst_err.find(NO_ERROR) == std::string::npos )
			{
				state  = Tango::ALARM;
				status = syst_err;
			}
			else
			{
				state  = Tango::STANDBY;
				status = "Ready to read back data.";
			}
		}
	}
	catch(Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		Tango::Except::re_throw_exception (df,
			"COMMUNICATION_ERROR",
			"Check logs to have the error description.",
			"KeithleyManager::get_error");
	}
	catch(...)
	{
		ERROR_STREAM << "KeithleyManager::get_error -> caught [...] exception." << std::endl;
		Tango::Except::throw_exception ("UNKNOW_ERROR",
			"Caught a generic exception !",
			"KeithleyManager::get_error");
	}
	
	{//- critical section
	  yat::MutexLock gard(m_stat_mutex);
    m_state = state;
    m_status= status;
	}
}

// ===================================================================
// KeithleyManager::getStatus
// ===================================================================
std::string KeithleyManager::getStatus()
{
  yat::MutexLock gard(m_stat_mutex);
	return m_status;
}

// ===================================================================
// KeithleyManager::getState
// ===================================================================
Tango::DevState KeithleyManager::getState()
{
  yat::MutexLock gard(m_stat_mutex);
	return m_state;
}

/// ============================================================================
// KeithleyManager::send
// ============================================================================
// void KeithleyManager::send(std::string cmd)
// {
// 	if ( !m_comLink )
// 	{
// 		m_state  = Tango::FAULT;
// 		m_status = "No communication link built !";
// 		return;
// 	}

// 	if ( cmd.empty() )
// 	{
// 		m_error = "Invalid parameter : argin cannot be empty !";
// 		return;
// 	}

// 	try
// 	{
// 		send_receive(cmd);
// 		m_error = NO_ERROR;
// 	}
// 	catch(Tango::DevFailed & df)
// 	{
// 		ERROR_STREAM << df << std::endl;
// 		m_error = "Send cmd -> Caught a DevFailed : Check logs to have the error description.";
// 	}
// 	catch(...)
// 	{
// 		ERROR_STREAM << "KeithleyManager::send -> caught [...] exception." << std::endl;
// 		m_error = "Send cmd -> Caught a generic exception !";
// 	}
// }

// ============================================================================
// KeithleyManager::receive
// ============================================================================
// std::string KeithleyManager::receive()
// {
// 	std::string response("");

// 	if ( !m_comLink )
// 	{
// 		m_state  = Tango::FAULT;
// 		m_status = "No communication link built !";
// 		return m_status;
// 	}

// 	try
// 	{
// 		response = send_receive();
// 		m_error = NO_ERROR;
// 	}
// 	catch(Tango::DevFailed & df)
// 	{
// 		ERROR_STREAM << df << std::endl;
// 		m_error = "receive cmd -> Caught a DevFailed : Check logs to have the error description.";
// 	}
// 	catch(...)
// 	{
// 		ERROR_STREAM << "KeithleyManager::receive -> caught [...] exception." << std::endl;
// 		m_error = "receive cmd -> Caught a generic exception !";
// 	}

// 	return response;
// }

// ===================================================================
// KeithleyManager::readBufferedData
// ===================================================================
void KeithleyManager::readBufferedData()
{
	if ( !m_comLink )
	{
		Tango::Except::throw_exception ("BAD_INITIALIZATION",
			"No communication link built!",
			"KeithleyManager::readBufferedData");
	}

	// {
	// 	//- check buffered data size
	// 	std::string buff_size_str = send_receive(ACTUAL_BUFF_SIZE_CMD);
	// 	yat::MutexLock gard(m_data_mutex);
	// 	m_buffSize = yat::XString<size_t>::to_num(buff_size_str);
	// }
//- std::cout << "\n\t\t KeithleyManager::readBufferedData() -> ..." << std::endl;

	//- clear previous converted data
	{
		yat::MutexLock gard(m_data_mutex);
		//- clear previous raw data (Keithley send data in ASCII)
		m_bufferedDataStr.clear();
		m_nbData = 0;
    m_buffSize = 0;
		m_cmd_getData_sent = false;
		m_vector_data.clear();
	}
//- std::cout << "\t\t KeithleyManager::readBufferedData() -> RESET m_nbData = " << m_nbData << std::endl;
  
  if( m_keithley_data )
  	m_keithley_data->clear_data();

  yat::Message* msg = yat::Message::allocate(READ_BUFFERED_DATA_MSG);
  post(msg);
//- std::cout << "\t\t KeithleyManager::readBufferedData() -> POST" << std::endl;
}

// ===================================================================
// KeithleyManager::upload_buffered_data
// ===================================================================
void KeithleyManager::upload_buffered_data()
{
  int status=0;
  //- VECTORS
  std::vector<double> voltage;
  static std::size_t total_size = 0;
  std::string data("");
yat::Timer t;

//- std::cout << "\n\n\tKeithleyManager::handle_message entering handling of upload_buffered_data" << std::endl;
  try
  {

//- std::cout << "\t\t m_buffSize = " << m_buffSize << " & m_nbData = " << m_nbData << std::endl;
    if ( m_buffSize < m_nbData )
    {
      //- check buffered data size
      update_nb_stored_data();
      // std::string buff_size_str = send_receive(ACTUAL_BUFF_SIZE_CMD);
      // m_buffSize = yat::XString<size_t>::to_num(buff_size_str);

//- std::cout << "\t\t m_bufferedDataStr size = " << m_bufferedDataStr.size() 
    // << " actual size = " << m_buffSize
    // << " expecting : " << m_nbData
    // << std::endl;
    }
    
    //- all data are stored
    if ( m_buffSize == m_nbData )
    {
    //- send cmd to read buffer
      if (!m_cmd_getData_sent )
      {
        yat::MutexLock gard(m_com_mutex);
// std::cout << "\n\t\tSending cmd..." << std::endl;
        data = send_receive(READ_DATA_CMD);
// std::cout << "\t\t CDM sent -> received data size = " << data.size() << std::endl;
        total_size += data.size();
//- std::cout << "\t\t First data [" << data << "]" << std::endl;
        m_bufferedDataStr += data;
        m_cmd_getData_sent = true;
//- std::cout << "\t\tREAD data cmd SENT size = " << m_bufferedDataStr.size() << std::endl;
				set_periodic_msg_period(3000);
      }
      else
      {
//- std::cout << "\t\t Reading last buffered data..." << std::endl;
				try
				{
	        data = send_receive();
				}
				catch(...)
				{
					//- no more data to read!!!
          m_acquisition_on_the_way = false;
					set_periodic_msg_period(kPERIODIC_TMOUT_MS);
//- std::cout << "\t\t Reading last buffered data... NO MORE DATA!!!" << std::endl;
				}
					
//           if ( !data.empty() )
//           {
//           yat::MutexLock gard(m_com_mutex);
// std::cout << "\t\t not complete data received!!"  << std::endl;
//           total_size += data.size();
// std::cout << "\t\t total data size = " << total_size << std::endl;
//             m_bufferedDataStr += data;
//           }
//           else
//           {
// std::cout << "\t\t parsing data..." << std::endl;
//             m_bufferedDataStr += data;
//             //- now buffer is empty!

// std::cout << "\t\t received data size : " << m_bufferedDataStr.size() << std::endl;
//             status = parse(m_bufferedDataStr, voltage);
// std::cout << "\t\t time to PARSE data : " << t.elapsed_msec() << " milliseconds" << std::endl;
//             INFO_STREAM << "\t\t time to PARSE data : " << t.elapsed_msec() << " milliseconds" << std::endl;
//             {
//               yat::MutexLock gard(m_data_mutex);
//               m_vector_data.assign(voltage.begin(), voltage.end());
//               total_size = 0;
            
// std::cout << "\t\t ASSIGNED " << voltage.size() << " data in deque with " << m_vector_data.size() << " data." << std::endl;

//             //- now data are acquired -> allow internal data update
//             m_acquisition_on_the_way = false;
//             // enable_periodic_msg(false);
//           }
        // }
      }
	    if( m_keithley_data )
	    {
	    	m_keithley_data->parse(data);
	    }
//- std::cout << "\t\t if == " << std::endl;
    }
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << "\t\t Periodic DF : \n" << df << std::endl;
//- std::cout << "\t\t DEV FAILED Periodic DF!" << std::endl;
    // enable_periodic_msg(false);
  }
  catch(...)
  {
    ERROR_STREAM << "\t\t ERROR : Generic exception caught while reading/parsing buffered data!" << std::endl;
std::cout << "\t\t ERROR : Generic exception caught while reading/parsing buffered data!" << std::endl;
    // enable_periodic_msg(false);
  }
//- std::cout << "\t\t time to ASSIGN data : " << t.elapsed_msec() << " milliseconds" << std::endl;
}

// ===================================================================
// KeithleyManager::get_freshData
// ===================================================================
std::vector<double>& KeithleyManager::get_freshData()
{
	if( m_keithley_data )
	{
		m_vector_data = m_keithley_data->get_values();
	}
	// yat::MutexLock gard(m_data_mutex);
	return m_vector_data;
}

// ===================================================================
// KeithleyManager::is_integrating
// ===================================================================
bool  KeithleyManager::is_integrating()
{
	bool integrate = false;
	{
		yat::MutexLock gard(m_com_mutex);
		integrate = periodic_msg_enabled();
	}
	return ( integrate );
}

//--------------------------------------------------------------------
//-                    INTERNAL METHODS
//--------------------------------------------------------------------
// ===================================================================
// KeithleyManager::create_proxy
// ===================================================================
void KeithleyManager::create_proxy(std::string communicationLinkName)
{
	try
	{
		//- check proxy name
		if ( communicationLinkName.empty() || communicationLinkName.find("noCom") != std::string::npos)
		{
			m_comLink = 0;
			m_error  = "KeithleyManager::No device communicationLinkName property set (or bad formatted).";
			m_state  = Tango::FAULT;
			m_status = m_error;
      //- std::cout << m_error << std::endl;
		}
		else
		{
			//- create proxy for communication
			m_comLink = new Communication(m_hostDev, communicationLinkName);
			m_comLink->create_proxy();
      //- std::cout << "KeithleyManager:: New COM" << std::endl;
			//- check device is connected and up
			m_comLink->write_read(IDENTIFICATION_CMD);
      //- std::cout << "KeithleyManager:: IDN" << std::endl;
			m_error = NO_ERROR;
		}
	}
	catch(Tango::DevFailed& df)
	{
		delete_proxy();
		m_error = "KeithleyManager::create_proxy DF FAILED to create proxy on : " + communicationLinkName + " caught DevFailed.";
		FATAL_STREAM << df << std::endl;
		m_state  = Tango::FAULT;
		m_status = m_error;
      //- std::cout << m_error << std::endl;
	}
	catch(...)
	{
		delete_proxy();
		m_error = "KeithleyManager::create_proxy [...] FAILED to create proxy on : " + communicationLinkName + " caught [...].";
		FATAL_STREAM << m_error << std::endl;
		m_state  = Tango::FAULT;
		m_status = m_error;
      //- std::cout << m_error << std::endl;
	}
}

// ============================================================================
// KeithleyManager::delete_proxy
// ============================================================================
void KeithleyManager::delete_proxy()
{
	if ( m_comLink )
	{
		delete m_comLink;
		m_comLink = 0;
	}
}

// ===================================================================
// KeithleyManager::write_read
// ===================================================================
std::string KeithleyManager::write_read(std::string cmd_to_send)
{
	WriteRead_Str data_str;
  bool waitable = true;

  yat::Message* msg = yat::Message::allocate(WRITE_READ_MSG, MAX_USER_PRIORITY, waitable);
  data_str.cmd_to_send = cmd_to_send;
  msg->attach_data(data_str);
  wait_msg_handled(msg->duplicate(), kMSG_TIMEOUT);

  //- prepare command response
	// WriteRead_Str data_out;
	data_str = msg->get_data<WriteRead_Str>();
	std::string resp = data_str.response;
	//- 
	msg->release();

  return resp;
}

// ============================================================================
// KeithleyManager::send_receive
// ============================================================================
std::string KeithleyManager::send_receive (std::string cmd_to_send, size_t sleep)
{
	std::string response("");
	if ( !m_comLink )
	{
		m_state  = Tango::FAULT;
		m_status = "No communication link built!";
		return response;
		// Tango::Except::throw_exception ("BAD_INITIALIZATION",
		// 	"No comunication link built!",
		// 	"KeithleyManager::send_receive");
	}

	try
	{
			yat::MutexLock gard(m_com_mutex);
		//- is it a write cmd ?
		if (  !cmd_to_send.empty() )
		{
			m_comLink->write(cmd_to_send);
			response = "No response for command : " + cmd_to_send;
		}

		//- if cmd is empty, that means it has been sent previously so read the response
		//- or the command sent returns a response, so read it now !!!
		if ( cmd_to_send.empty() || cmd_to_send.rfind("?") != std::string::npos )
		{
      omni_thread::sleep(0, sleep*1000000);
			response = m_comLink->read();
		}
	}
	catch(Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		Tango::Except::re_throw_exception (df,
			"COMMUNICATION_ERROR",
			"Check logs to have the error description.",
			"KeithleyManager::send_receive");
	}
	catch(...)
	{
		ERROR_STREAM << "KeithleyManager::send_receive -> caught [...] exception." << std::endl;
		Tango::Except::throw_exception ("UNKNOW_ERROR",
			"Caught a generic exception !",
			"KeithleyManager::send_receive");
	}

	return response;
}

// ============================================================================
// KeithleyManager::parse
// ============================================================================
// int KeithleyManager::parse(std::string& rcv, std::vector<double>& vVoltage)
// {
// 	//- Keithley data separator
// 	std::string delim(",");

// 	yat::StringTokenizer st(rcv, delim);

// 	try
// 	{
//     while(st.has_more_tokens())
//     {
//   		double val = st.next_fp_token();
//   		vVoltage.push_back(val);
//   	}

// 		m_error = NO_ERROR;
// 	}
// 	catch(...)
// 	{
// 		ERROR_STREAM << "KeithleyManager::parse -> caught [...] exception." << std::endl;
// 		m_error = "parse cmd -> Caught a generic exception !";
// 	}

// 	return(-1);
// }

} //- end namespace
