// ============================================================================
//
// = CONTEXT
//    TANGO Project - KeithleyMultimeters Support Library
//
// = FILENAME
//    KeithleyData.hpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _KEITHLEY_DATA_H_
#define _KEITHLEY_DATA_H_

/**
 *  \brief Class to manage 2700 Keithley data type
 *
 *  \author Xavier Elattaoui
 *  \date 11-2012
 */
#include <vector>
#include <string>
#include <yat4tango/DeviceTask.h>

class KeithleyData : public yat4tango::DeviceTask
{
public:

	/**
	*  \brief Initialization. 
	*/
	KeithleyData (Tango::DeviceImpl * devHost);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~KeithleyData ();
  
  void parse(std::string);

  void clear_data();

  std::vector< double > get_values();

protected:
  
  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  //- data mutex
  yat::Mutex m_data_mutex;
  yat::Mutex m_values_mutex;
  
  virtual void process_message (yat::Message& msg);

private:

	//- host device
  Tango::DeviceImpl * m_hostDev;

  std::string m_data_to_parse;

  std::vector< double > m_vec_data;

};

#endif // _KEITHLEY_DATA_H_
