// ============================================================================
//
// = CONTEXT
//    TANGO Project - KeithleyMultimeters Support Library
//
// = FILENAME
//    KeithleyManager.hpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

#ifndef _Keithley_MGR_H_
#define _Keithley_MGR_H_

/**
 *  \brief This class manage 2700 Keithley type
 *
 *  \author Xavier Elattaoui
 *  \date 11-2012
 */
#include <tango.h>
#include <string>
#include <vector>
#include <DeviceProxyHelper.h>
#include <yat4tango/DeviceTask.h>
#include "Communication.hpp"
#include "KeithleyData.hpp"


namespace KeithleyMultimeters_ns
{

class KeithleyManager : public yat4tango::DeviceTask
{
public:

	/**
	*  \brief Initialization. 
	*/
	KeithleyManager (std::string& comLink_device_name,
                   std::vector<std::string>& config,
                   Tango::DeviceImpl * devHost);
	
	/**
	*  \brief Release resources.
	*/
	virtual ~KeithleyManager (void);
  
  //- update state/status
  void update_state_status();
  
	/**
  *  \brief state/status.
  */
  std::string			getStatus();
  Tango::DevState getState ();

  //- Keithley integration time (Number of Power Line Cycle)
  void set_nplc (double);
  double get_nplc ();

  //- Keithley trigger source (Power Line Cycle)
  void set_trigger_source (std::string);
  std::string get_trigger_source ();

  /**
  *  \brief HW direct access (public).
  */
	//- Direct command from/to the connected keithley
  std::string write_read (std::string);

/******************************
*   BUFFERIZED DATA MANAGEMENT
*******************************/
  //- Read back Keithley stored data
	void readBufferedData ();

	//- returns buffered data
  std::vector<double>& get_freshData ();

  /**
  *  \brief Getters/Setters.
  */
  bool  is_integrating ();

  //- Keithley buffer size
  std::size_t get_buffer_size ();
  void set_buffer_size (std::size_t);

  //- Keithley nb stored data
  std::size_t get_actual_nbPoints ();
  
  //- Configure acquisition
  void configure_scan ();

  //- Start new acquisition
  void start_scan ();

  //- Stop/Abort current acquisition
  void abort_scan ();

protected:

	//- process_message (implements yat4tango::DeviceTask pure virtual method)
 	//- data mutex
	yat::Mutex m_data_mutex; 
	//- hw access mutex
 	yat::Mutex m_com_mutex; 
	//- state/status mutex
 	yat::Mutex m_stat_mutex; 
	
	virtual void process_message (yat::Message& msg)
									throw (Tango::DevFailed);

private:

	//- Proxy management
  void create_proxy (std::string);
  void delete_proxy ();

  //- Keithley configuration
  void configure ();

  //- delete last data
  void reset_stored_data ();

  //- read back stored data
  void upload_buffered_data ();

	//- parse buffered data 
	// int parse (std::string& rcv, vector<double>& vVoltage);

  //- update state/status
  void get_error();

  //- update internal data
  void update_internal_data();

  //- update internal Keithley buffer size
  void update_nb_stored_data();

  //- Direct command from/to the connected keithley
  std::string send_receive (std::string cmd_to_send = "", size_t sleep=20);
  // void send (std::string);
  // std::string receive ();

	//- host device
  Tango::DeviceImpl *	m_hostDev;
	//- communication device link proxy
  Communication *	    m_comLink;
	//- communication device link name
  std::string         m_comName;
  //- data parser/converter
  KeithleyData *      m_keithley_data;

  //- state/status management
  Tango::DevState m_state;
  std::string     m_status;
  std::string     m_error;

  //- user keithley configuration
  std::vector<std::string> m_user_config;
  
  bool m_bufferise_data;

  //- Keithley returned stored data
	std::string m_bufferedDataStr;

	//- data management
	std::vector<double> m_vector_data;
  std::size_t m_nbData;
	std::size_t m_buffSize;
  double m_nplc;
  std::string m_triggerSrc;

  //- Send command to read back stored data once!
	bool m_cmd_getData_sent;

  //- do not allow updates while reading back data!
  bool m_acquisition_on_the_way;

  //- Structure used with YAT message
  //-   to send a command and read back the reponse
  struct WriteRead_Str
  {
    std::string cmd_to_send;  //- Keithley direct command
    std::string response;     //- Keithley command response
  };
};

} //- end namespace

#endif // _Keithley_MGR_H_
