/*----- PROTECTED REGION ID(KeithleyMultimetersClass.h) ENABLED START -----*/
//=============================================================================
//
// file :        KeithleyMultimetersClass.h
//
// description : Include for the KeithleyMultimeters root class.
//               This class is the singleton class for
//                the KeithleyMultimeters device class.
//               It contains all properties and methods which the 
//               KeithleyMultimeters requires only once e.g. the commands.
//
// project :     Keithley multimeters device
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef KeithleyMultimetersClass_H
#define KeithleyMultimetersClass_H

// #include <tango.h>
#include "KeithleyMultimeters.h"

/*----- PROTECTED REGION END -----*/	//	KeithleyMultimetersClass.h


namespace KeithleyMultimeters_ns
{
/*----- PROTECTED REGION ID(KeithleyMultimetersClass::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	KeithleyMultimetersClass::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
//	Attribute nPLC class definition
class nPLCAttrib: public Tango::Attr
{
public:
	nPLCAttrib():Attr("nPLC",
			Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~nPLCAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<KeithleyMultimeters *>(dev))->read_nPLC(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<KeithleyMultimeters *>(dev))->write_nPLC(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<KeithleyMultimeters *>(dev))->is_nPLC_allowed(ty);}
};

//	Attribute triggerSource class definition
class triggerSourceAttrib: public Tango::Attr
{
public:
	triggerSourceAttrib():Attr("triggerSource",
			Tango::DEV_STRING, Tango::READ_WRITE) {};
	~triggerSourceAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<KeithleyMultimeters *>(dev))->read_triggerSource(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<KeithleyMultimeters *>(dev))->write_triggerSource(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<KeithleyMultimeters *>(dev))->is_triggerSource_allowed(ty);}
};

//	Attribute nbPoints class definition
class nbPointsAttrib: public Tango::Attr
{
public:
	nbPointsAttrib():Attr("nbPoints",
			Tango::DEV_ULONG, Tango::READ_WRITE) {};
	~nbPointsAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<KeithleyMultimeters *>(dev))->read_nbPoints(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<KeithleyMultimeters *>(dev))->write_nbPoints(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<KeithleyMultimeters *>(dev))->is_nbPoints_allowed(ty);}
};

//	Attribute nbBufferizedPoints class definition
class nbBufferizedPointsAttrib: public Tango::Attr
{
public:
	nbBufferizedPointsAttrib():Attr("nbBufferizedPoints",
			Tango::DEV_ULONG, Tango::READ) {};
	~nbBufferizedPointsAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<KeithleyMultimeters *>(dev))->read_nbBufferizedPoints(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<KeithleyMultimeters *>(dev))->is_nbBufferizedPoints_allowed(ty);}
};

//	Attribute data class definition
class dataAttrib: public Tango::SpectrumAttr
{
public:
	dataAttrib():SpectrumAttr("data",
			Tango::DEV_DOUBLE, Tango::READ, 450000) {};
	~dataAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<KeithleyMultimeters *>(dev))->read_data(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<KeithleyMultimeters *>(dev))->is_data_allowed(ty);}
};


//=========================================
//	Define classes for commands
//=========================================
//	Command ReadBufferedData class definition
class ReadBufferedDataClass : public Tango::Command
{
public:
	ReadBufferedDataClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ReadBufferedDataClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ReadBufferedDataClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<KeithleyMultimeters *>(dev))->is_ReadBufferedData_allowed(any);}
};

//	Command WriteRead class definition
class WriteReadClass : public Tango::Command
{
public:
	WriteReadClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	WriteReadClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~WriteReadClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<KeithleyMultimeters *>(dev))->is_WriteRead_allowed(any);}
};

//	Command Abort class definition
class AbortClass : public Tango::Command
{
public:
	AbortClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	AbortClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~AbortClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<KeithleyMultimeters *>(dev))->is_Abort_allowed(any);}
};

//	Command Configure class definition
class ConfigureClass : public Tango::Command
{
public:
	ConfigureClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ConfigureClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ConfigureClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<KeithleyMultimeters *>(dev))->is_Configure_allowed(any);}
};

//	Command StartAcquisition class definition
class StartAcquisitionClass : public Tango::Command
{
public:
	StartAcquisitionClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StartAcquisitionClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StartAcquisitionClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<KeithleyMultimeters *>(dev))->is_StartAcquisition_allowed(any);}
};


/**
 *	The KeithleyMultimetersClass singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  KeithleyMultimetersClass : public Tango::DeviceClass
#else
class KeithleyMultimetersClass : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(KeithleyMultimetersClass::Additionnal DServer data members) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyMultimetersClass::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
	
		//	Method prototypes
		static KeithleyMultimetersClass *init(const char *);
		static KeithleyMultimetersClass *instance();
		~KeithleyMultimetersClass();
		Tango::DbDatum	get_class_property(string &);
		Tango::DbDatum	get_default_device_property(string &);
		Tango::DbDatum	get_default_class_property(string &);
	
	protected:
		KeithleyMultimetersClass(string &);
		static KeithleyMultimetersClass *_instance;
		void command_factory();
		void attribute_factory(vector<Tango::Attr *> &);
		void write_class_property();
		void set_default_property();
		void get_class_property();
		string get_cvstag();
		string get_cvsroot();
	
	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,vector<Tango::Attr *> &);
		vector<string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname);
};

}	//	End of namespace

#endif   //	KeithleyMultimeters_H
