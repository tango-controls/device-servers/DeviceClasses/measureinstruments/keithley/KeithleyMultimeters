// ============================================================================
//
// = CONTEXT
//    TANGO Project - KeithleyMultimeters Support Library
//
// = FILENAME
//    KeithleyData.cpp
//
// = AUTHOR
//    X. Elattaoui
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/utils/StringTokenizer.h>  //- to parse data
#include <yat/utils/XString.h>  				//- for data conversion (str -> num)
#include "KeithleyData.hpp"

// ----------------------------------------------------------------------------
//- messages
// ----------------------------------------------------------------------------
//- the YAT user messages 
const std::size_t EXTRACT_DATA	= yat::FIRST_USER_MSG + 10;

// ----------------------------------------------------------------------------
//- constantes
// ----------------------------------------------------------------------------
const std::size_t MAX_DATA_SIZE = 450000;
// ----------------------------------------------------------------------------

// ============================================================================
// KeithleyData::KeithleyData
// ============================================================================
KeithleyData::KeithleyData (Tango::DeviceImpl * host_dev)
:	yat4tango::DeviceTask(host_dev),
	m_hostDev(host_dev),
	m_data_to_parse("")
{
	//- std::cout << "\t\tKeithleyData::KeithleyData <-" << std::endl;

	m_vec_data.clear();
	m_vec_data.reserve(MAX_DATA_SIZE);

	//- std::cout << "\t\tKeithleyData::KeithleyData ->" << std::endl;
}

// ============================================================================
// KeithleyData::~KeithleyData
// ============================================================================
KeithleyData::~KeithleyData (void)
{
	//- std::cout << "\t\tKeithleyData::~KeithleyData <-" << std::endl;

	//- std::cout << "\t\tKeithleyData::~KeithleyData ->" << std::endl;
}

// ============================================================================
//- the user core of the Task
// ============================================================================
void KeithleyData::process_message (yat::Message& _msg)
{
	//- The DeviceTask's lock_ -------------
	DEBUG_STREAM << "KeithleyData::process_message::receiving msg " << _msg.to_string() << std::endl;

	//- handle msg
	switch (_msg.type())
	{
		//- THREAD_INIT =======================
	case yat::TASK_INIT:
		{
			DEBUG_STREAM << "KeithleyData::handle_message::THREAD_INIT::thread is starting up" << std::endl;
			//- "initialization" code goes here
			//- configure task
			enable_timeout_msg(false);
			enable_periodic_msg(false); //- no periodic!

			INFO_STREAM << " KeithleyData::handle_message handling TASK_INIT KeithleyData initialised. " << std::endl;
//- std::cout << " KeithleyData::handle_message handling TASK_INIT KeithleyData initialised. " << std::endl;
		}
		break;
		//- TASK_EXIT =======================
	case yat::TASK_EXIT:
		{
			INFO_STREAM << "KeithleyData::handle_message handling TASK_EXIT thread is quitting ..." << std::endl;
		}
		break;
		//- TASK_PERIODIC ===================
	case yat::TASK_PERIODIC:
		{

		}
		break;
    //- USER_DEFINED_MSG ================
  case EXTRACT_DATA:
    {
    	std::string data_to_conv("");
    	std::string key(",");
    	std::size_t beg = 0;
    	std::size_t idx = 0;

			{//- critical sectin
				yat::MutexLock gard(m_data_mutex);
			 	//- find last token
				idx = m_data_to_parse.rfind(key);
				//- extract string data to convert to num
				data_to_conv = m_data_to_parse.substr(beg, idx);
				//- keep remaining data
				m_data_to_parse = m_data_to_parse.substr(idx+1);
//- std::cout << "\t\t\t EXTRACT_DATA : data_to_conv size = " << data_to_conv.size() << std::endl;
			}
			
			//- Keithley data separator
			std::string delim(",");
			yat::StringTokenizer st(data_to_conv, delim);

			try
			{
		    while(st.has_more_tokens())
		    {
		  		double val = st.next_fp_token();
		  		{
						yat::MutexLock gard(m_values_mutex);
			  		m_vec_data.push_back(val);
			  	}
		  	}
//- std::cout << "\t\t\t EXTRACT_DATA : vector size = " << m_vec_data.size() << std::endl;
			}
			catch(...)
			{
				ERROR_STREAM << "KeithleyManager::parse -> caught [...] exception." << std::endl;
			}
    }
    break;
	}//- switch (_msg.type())
} //- KeithleyData::handle_message

// ============================================================================
// KeithleyData::parse
// ============================================================================
void KeithleyData::parse(std::string raw_data)
{
	{//- critical sectin
		yat::MutexLock gard(m_data_mutex);
		m_data_to_parse += raw_data;
	}

  yat::Message* msg = yat::Message::allocate(EXTRACT_DATA);
  post(msg);

}

// ============================================================================
// KeithleyData::clear_data
// ============================================================================
void KeithleyData::clear_data()
{
	{//- critical sectin
		yat::MutexLock gard(m_data_mutex);
		m_data_to_parse.clear();
		m_vec_data.clear();
	}
}

// ============================================================================
// KeithleyData::get_values
// ============================================================================
std::vector< double > KeithleyData::get_values()
{
	{//- critical sectin
		yat::MutexLock gard(m_values_mutex);
		return m_vec_data;
	}
}

