// ============================================================================
//
// = CONTEXT
//    TANGO Project - Communication Class
//
// = FILENAME
//    Communication.hpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#ifndef _COM_CLASS_H_
#define _COM_CLASS_H_

#include <tango.h>
#include <string>
#include <DeviceProxyHelper.h>
#include <yat4tango/LogHelper.h>

/**
 *  \brief Class to manage communication link
 *
 *  \author Xavier Elattaoui
 *  \date 03-2020
 */
namespace KeithleyMultimeters_ns
{

class Communication : yat4tango::TangoLogAdapter
{
public :
  
  /**
  *  \brief Initialization.
  */
  Communication (Tango::DeviceImpl * host_device, std::string comDevName);

  /**
  *  \brief Release resources.
  */
  virtual ~Communication ();

  /**
  *  \brief Sends command and returns the response.
  */
  std::string write_read(std::string);
  
  /**
  *  \brief Sends command
  */
  void write(std::string);
  
  /**
  *  \brief Read back response
  */
  std::string read();
  
  void create_proxy();

  /**
  *  \brief Checks proxy creation.
  */
  bool is_proxy_ok() {
  	return m_dsproxy ? true : false;
  }
  
  /**
  *  \brief Returns communication errors if any.
  */
  std::string get_com_error() {
  	return m_error;
  }
  
  Tango::DevState get_com_state() {
    return m_com_state;
  }
  
  /**
  *  \brief Clears errors.
  */
  void clear_error();

private :
  
  void delete_proxy();
  
  void check_proxy();
  
  //- proxy
  Tango::DeviceProxyHelper* m_dsproxy;

  //- the host device
  Tango::DeviceImpl*        m_host_dev;
  
  //- communication device name
  std::string               m_dev_name;

  //- controller response
  std::string               m_response;
  
  //- errors
  std::string 		          m_error;
  
  //- communication state
  Tango::DevState           m_com_state;
  
};

} //- end namespace

#endif // _COM_CLASS_H_

