/**
 * Device Class Identification:
 * 
 * Class Name   :	KeithleyMultimeters
 * Contact      :	xavier.elattaoui@synchrotron-soleil.fr
 * Class Family :	Instrumentation
 * Platform     :	All Platforms
 * Bus          :	GPIB
 * Manufacturer :	Keithley
 * Reference    :	2700
 */
