// ============================================================================
//
// = CONTEXT
//    TANGO Project - Communication Class
//
// = FILENAME
//    Communication.cpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#include "Communication.hpp"

// ----------------------------------------------------------------------------
//- (Serial) commands
static const std::string WRITE_CMD          = "Write";
static const std::string READ_CMD           = "Read";
static const std::string WRITE_READ_CMD     = "WriteRead";
static const std::string OPEN_COM_CMD       = "Connect";

namespace KeithleyMultimeters_ns
{
// ============================================================================
// Communication::Communication
// ============================================================================
Communication::Communication (Tango::DeviceImpl * host_device,
                              std::string comDevName)
: yat4tango::TangoLogAdapter(host_device),
  m_dsproxy (0),
  m_host_dev(host_device),
  m_dev_name(comDevName),
  m_response(""),
  m_error(""),
  m_com_state(Tango::INIT)
{
  //- Noop
}

// ============================================================================
// Communication::~Communication
// ============================================================================
Communication::~Communication (void)
{
  delete_proxy();
}

// ============================================================================
// Communication::create_proxy
// ============================================================================
void Communication::create_proxy()
{
  if ( !m_dev_name.empty() )
  {
    try
    {
      //- create proxy
      m_dsproxy = new Tango::DeviceProxyHelper(m_dev_name, m_host_dev);
      m_com_state = Tango::ON;
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::FAULT;
      m_error = "Communication::Failed to create proxy on \"" + m_dev_name + "\" : \n";
      m_error+= std::string(df.errors[0].desc);
      //- std::cout << m_error << std::endl;
      delete_proxy();
      return;
    }
  }
}

// ============================================================================
// Communication::delete_proxy
// ============================================================================
void Communication::delete_proxy()
{
  if ( m_dsproxy )
  {
    delete m_dsproxy;
    m_dsproxy = 0;
  }
}

// ============================================================================
// Communication::check_proxy
// ============================================================================
void Communication::check_proxy()
{
  if ( m_dsproxy )
  {
    try
    {
      //- check device is up !
      m_dsproxy->get_device_proxy()->ping();
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::ALARM;
      //- store the Tango error
      m_error = std::string(df.errors[0].desc);
      ERROR_STREAM << m_error << std::endl;
      throw;
    }
    
    try
    {
      //- read status to get updated state!
      m_dsproxy->get_device_proxy()->status();
      //- check device is up !
      m_com_state = m_dsproxy->get_device_proxy()->state();
      if( m_com_state != Tango::OPEN )
      {
        m_dsproxy->command(OPEN_COM_CMD);
        m_com_state = m_dsproxy->get_device_proxy()->state();
      	m_error = "Cannot read back state of device \"" + m_dev_name + "\" or its Tango state is not OPEN.\n";
      }
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::ALARM;
      m_error = "Cannot read back state of device \"" + m_dev_name + "\" \n";
      m_error+= std::string(df.errors[0].desc);
      ERROR_STREAM << m_error << std::endl;
      throw;
    }
  }
}

// ============================================================================
// Communication::write
// ============================================================================
void Communication::write(std::string cmd_to_send)
{
  m_dsproxy->command_in(WRITE_CMD, cmd_to_send);
}

// ============================================================================
// Communication::read
// ============================================================================
std::string Communication::read()
{
  std::string response("");
  
  m_dsproxy->command_out(READ_CMD, response);

  return response;
}

// ============================================================================
// Communication::write_read
// ============================================================================
std::string Communication::write_read(std::string cmd)
{
  static std::string empty("");
  std::string response("");

  if ( !m_dsproxy )
  {
    m_com_state = Tango::FAULT;
    m_error = "Communication link proxy is not created.";
    Tango::Except::throw_exception("COMMUNICATION_ERROR",
                                   m_error,
                                   "Communication::write_read");
  }

  try
  {
    check_proxy();

    //- send the cmd 
    m_dsproxy->command_inout(WRITE_READ_CMD, cmd, response);
  }
  catch(...)
  {
  	return empty;
  }

  return m_response;
}

// ============================================================================
// Communication::clear_error
// ============================================================================
void Communication::clear_error()
{
  if ( !m_dsproxy )
  {
    Tango::Except::throw_exception("COMMUNICATION_ERROR",
                                   "Communication link proxy is not created.",
                                   "Communication::clear_error");
  }

  m_com_state = Tango::ON;
  m_error.clear();
}

} //- end namespace
